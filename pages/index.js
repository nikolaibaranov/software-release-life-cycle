import Head from 'next/head'
import styles from '../styles/Home.module.css'

const Home = () => {

  const onClick = () => {
    console.log('click');
    throw new Error("Test Sentry Error");
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>Software release life cycle Example</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <a onClick={onClick} className={styles.button}>Click</a>
      </main>
    </div>
  )
}

export default Home;