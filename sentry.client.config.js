import * as Sentry from '@sentry/nextjs';

const SENTRY_DSN = process.env.SENTRY_DSN || process.env.NEXT_PUBLIC_SENTRY_DSN;

Sentry.init({
  dsn: SENTRY_DSN || 'https://439f76d4372e46ec934f1ec1d58bb52a@o575353.ingest.sentry.io/5872769',
  // Adjust this value in production, or use tracesSampler for greater control
  tracesSampleRate: 1.0,
});
