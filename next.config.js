const { withSentryConfig } = require('@sentry/nextjs');
const nextBuildId = require('next-build-id');
const { version } = require('./package.json');

let buildId = version;
try {
  buildId = nextBuildId.sync({ dir: __dirname });
} catch (exception) {
  buildId = version;
  console.warn('git rev-parse is unavailable, rollback to package.json version');
}

const moduleExports = {
  reactStrictMode: true,
  env: {
    NEXT_PUBLIC_BUILD_ID: buildId,
  },
  generateBuildId: () => buildId,
};

const SentryWebpackPluginOptions = {
  silent: true,
};

module.exports = withSentryConfig(moduleExports, SentryWebpackPluginOptions);
